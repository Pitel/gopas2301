package cz.gopas.kalkulacka.calc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import cz.gopas.kalkulacka.R;
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding;
import cz.gopas.kalkulacka.history.HistoryEntity;
import cz.gopas.kalkulacka.history.HistoryViewModel;

public final class CalcFragment extends Fragment {
    private static final String TAG = CalcFragment.class.getSimpleName();

    private FragmentCalcBinding binding = null;

    private CalcViewModel viewModel;
    private HistoryViewModel historyViewModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(CalcViewModel.class);
        historyViewModel = new ViewModelProvider(requireActivity()).get(HistoryViewModel.class);
    }

    @NonNull
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        binding = FragmentCalcBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding.calc.setOnClickListener(v -> calc());
        binding.share.setOnClickListener(v -> share());
        binding.ans.setOnClickListener(v -> viewModel.getAns());
        binding.history.setOnClickListener(v -> Navigation.findNavController(requireView()).navigate(R.id.action_calcFragment_to_historyFragment));
        viewModel.res.observe(getViewLifecycleOwner(), x -> {
            binding.res.setText(String.valueOf(x));
            final HistoryEntity entity = new HistoryEntity();
            entity.value = x;
            //TODO Fix duplicates
            historyViewModel.db.insert(entity);
        });
        viewModel.ans.observe(getViewLifecycleOwner(), ans -> {
            if (ans != null) {
                binding.aText.setText(String.valueOf(ans));
                viewModel.ans.setValue(null);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        historyViewModel.selected.observe(getViewLifecycleOwner(), value -> {
            if (value != null) {
                binding.bText.setText(String.valueOf(value));
                historyViewModel.selected.setValue(null);
            }
        });
    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }

    private void calc() {
        Log.d(TAG, "Calc");

        final float a = Float.parseFloat(binding.aText.getText().toString());
        final float b = Float.parseFloat(binding.bText.getText().toString());
        final int op = binding.ops.getCheckedRadioButtonId();

        if (op == R.id.div && b == 0) {
            Navigation.findNavController(requireView()).navigate(R.id.action_calcFragment_to_zeroDialogFragment);
        }

        viewModel.calc(a, b, op);
    }

    private void share() {
        Log.d(TAG, "Share");
        final Intent intent = new Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding.res.getText()))
                .setType("text/plain");
        startActivity(intent);
    }
}
