package cz.gopas.kalkulacka.calc;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

import cz.gopas.kalkulacka.R;

public final class CalcViewModel extends AndroidViewModel {
    private static final String ANS_KEY = "ans";

    private final MutableLiveData<Float> mutableRes = new MutableLiveData<>();
    public final LiveData<Float> res = mutableRes;

    private final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplication());
    public final MutableLiveData<Float> ans = new MutableLiveData<>();

    public CalcViewModel(@NonNull Application application) {
        super(application);
    }

    public void calc(float a, float b, @IdRes int op) {
        final float x;
        switch (op) {
            case (R.id.add):
                x = a + b;
                break;
            case (R.id.sub):
                x = a - b;
                break;
            case (R.id.mul):
                x = a * b;
                break;
            case (R.id.div):
                x = a / b;
                break;
            default:
                x = Float.NaN;
        }

        mutableRes.setValue(x);

        prefs.edit().putFloat(ANS_KEY, x).apply();
    }

    public void getAns() {
        ans.setValue(prefs.getFloat(ANS_KEY, Float.NaN));
    }
}
