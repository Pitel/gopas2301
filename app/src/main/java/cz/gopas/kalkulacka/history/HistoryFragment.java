package cz.gopas.kalkulacka.history;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cz.gopas.kalkulacka.R;

public final class HistoryFragment extends Fragment {
    public HistoryFragment() {
        super(R.layout.fragment_history);
    }

    private HistoryViewModel viewModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(requireActivity()).get(HistoryViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recycler = (RecyclerView) view;
        recycler.setHasFixedSize(true);
        final HistoryAdapter adapter = new HistoryAdapter(value -> {
            viewModel.selected.setValue(value);
            Navigation.findNavController(requireView()).popBackStack();
        });
        recycler.setAdapter(adapter);

//        final ArrayList<HistoryEntity> list = new ArrayList<>();
//        for (int i = 0; i < 100_000; i++) {
//            final HistoryEntity entity = new HistoryEntity();
//            entity.id = i;
//            entity.value = i;
//            list.add(entity);
//        }
//        adapter.submitList(list);

        viewModel.db.getAll().observe(getViewLifecycleOwner(), adapter::submitList);
    }
}
