package cz.gopas.kalkulacka.history;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

public final class HistoryViewModel extends AndroidViewModel {
    public HistoryViewModel(@NonNull Application application) {
        super(application);
    }

    final public MutableLiveData<Float> selected = new MutableLiveData<>();

    final public HistoryDao db = Room.databaseBuilder(getApplication(), HistoryDatabase.class, "history")
            .allowMainThreadQueries()
            .build()
            .historyDao();
}
