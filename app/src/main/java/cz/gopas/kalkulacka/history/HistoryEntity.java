package cz.gopas.kalkulacka.history;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class HistoryEntity {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public float value;
}
