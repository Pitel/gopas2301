package cz.gopas.kalkulacka.history;

public interface HistoryClick {
    void onClick(float value);
}
