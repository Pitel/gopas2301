package cz.gopas.kalkulacka.history;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class HistoryAdapter extends ListAdapter<HistoryEntity, HistoryAdapter.HistoryViewHolder> {

    private static final String TAG = HistoryAdapter.class.getSimpleName();

    private static final DiffUtil.ItemCallback<HistoryEntity> DIFF = new DiffUtil.ItemCallback<HistoryEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull HistoryEntity oldItem, @NonNull HistoryEntity newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull HistoryEntity oldItem, @NonNull HistoryEntity newItem) {
            return oldItem.id == newItem.id && oldItem.value == newItem.value;
        }
    };

    private final HistoryClick click;

    public HistoryAdapter(HistoryClick click) {
        super(DIFF);
        setHasStableIds(true);
        this.click = click;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).id;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "Create");
        return new HistoryViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        android.R.layout.simple_list_item_1,
                        parent,
                        false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        Log.v(TAG, "Bind " + position);
        final HistoryEntity item = getItem(position);
        holder.text.setText(String.valueOf(item.value));
        holder.itemView.setOnClickListener(v -> click.onClick(item.value));
    }

    class HistoryViewHolder extends RecyclerView.ViewHolder {
        final TextView text;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(android.R.id.text1);
        }
    }
}
