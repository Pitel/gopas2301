package cz.gopas.kalkulacka.history;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface HistoryDao {
    @Query("SELECT * FROM HistoryEntity")
    LiveData<List<HistoryEntity>> getAll();

    @Insert
    void insert(HistoryEntity... entities);
}
