package cz.gopas.kalkulacka.about;

import androidx.fragment.app.Fragment;

import cz.gopas.kalkulacka.R;

public class AboutFragment extends Fragment {
    public AboutFragment() {
        super(R.layout.fragment_about);
    }
}
