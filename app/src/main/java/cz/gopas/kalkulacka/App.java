package cz.gopas.kalkulacka;

import android.app.Application;

import com.google.android.material.color.DynamicColors;

public final class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DynamicColors.applyToActivitiesIfAvailable(this);
    }
}
